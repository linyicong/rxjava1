package cn.edu.neusoft.demo.topic4.rx;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;

/**
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-30 10:22
 */
public class Main {
    public static void main(String[] args) {

        Subscription subscription = Observable.create(new Observable.OnSubscribe<String>() {
            public void call(Subscriber<? super String> subscriber) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext("1");
                    subscriber.onNext("2");
                    subscriber.onCompleted();
                }
            }
            //map操作符将传下来的String再转成Integer
        }).map(new Func1<String, Integer>() {
            public Integer call(String s) {
                return Integer.valueOf(s);
            }
        }).subscribe(new Observer<Integer>() {
            public void onCompleted() {
                System.out.println("onCompleted");
            }

            public void onError(Throwable throwable) {
                System.out.println("onError");
            }

            public void onNext(Integer integer) {
                System.out.println("onNext:" + integer + " instanceOf " + integer.getClass());
            }
        });
        subscription.unsubscribe();
    }
}
