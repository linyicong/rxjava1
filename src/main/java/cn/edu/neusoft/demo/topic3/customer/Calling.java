package cn.edu.neusoft.demo.topic3.customer;

/**
 * 描述打电话这件事 ——Subscription
 * 用于取消挂掉电话和获取当前是否在打电话的状态
 *
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-29 22:23
 */
public interface Calling {
    void unCall();

    boolean isUnCalled();
}
