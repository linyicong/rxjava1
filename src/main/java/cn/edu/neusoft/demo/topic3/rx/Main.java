package cn.edu.neusoft.demo.topic3.rx;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

/**
 * rxjava1 最简单的实例代码
 *
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-29 22:45
 */
public class Main {
    public static void main(String[] args) {
        Subscription subscription = Observable.create(new Observable.OnSubscribe<String>() {
            public void call(Subscriber<? super String> subscriber) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext("rx");
                    subscriber.onNext("java");
                    subscriber.onCompleted();
                }
            }
        }).subscribe(new Subscriber<String>() {
            public void onCompleted() {
                System.out.println("onCompleted");
            }

            public void onError(Throwable throwable) {
                System.out.println("onError");
            }

            public void onNext(String message) {
                System.out.println("onCompleted:" + message);
            }
        });
        subscription.unsubscribe();
    }
}
