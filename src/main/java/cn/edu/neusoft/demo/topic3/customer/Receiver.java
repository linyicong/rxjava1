package cn.edu.neusoft.demo.topic3.customer;

/**
 * 接收信息的人（实现了Callee和Calling接口）——Subscriber
 * 接电话的人挂掉电话
 *
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-29 22:21
 */
public abstract class Receiver<T> implements Callee<T>, Calling {

    private volatile boolean unCalled;

    public void unCall() {
        this.unCalled = true;
    }

    public boolean isUnCalled() {
        return this.unCalled;
    }
}
