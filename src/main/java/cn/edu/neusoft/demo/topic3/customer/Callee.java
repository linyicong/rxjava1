package cn.edu.neusoft.demo.topic3.customer;

/**
 * 接电话的人（观察者）——Observer
 * 接收数据的一方，作为Caller的call方法的参数
 *
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-29 22:25
 */
public interface Callee<T> {
    void onCompleted();

    void onError(Throwable error);

    void onReceive(T t);
}
