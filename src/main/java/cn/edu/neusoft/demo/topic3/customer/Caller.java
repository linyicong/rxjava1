package cn.edu.neusoft.demo.topic3.customer;

/**
 * 打电话的人（被观察者）——Observable
 * 发射数据的一方，通过create方法创建，通过call方法去打给接电话的人（观察者）
 *
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-29 22:19
 */
public class Caller<T> {
    private final OnCall<T> onCall;

    public Caller(OnCall<T> onCall) {
        this.onCall = onCall;
    }

    public static <T> Caller<T> create(OnCall<T> onCall) {
        return new Caller<T>(onCall);
    }

    public Calling call(Receiver<T> receiver) {
        this.onCall.call(receiver);
        return receiver;
    }

    /**
     * 当打电话时会触发此接口调用 ——OnSubscribe
     * 作用于Caller，向接电话的人发送通话内容
     *
     * @param <T>
     */
    public interface OnCall<T> extends Action1<Receiver<T>> {

    }
}
