package cn.edu.neusoft.demo.topic3.customer;

/**
 * 模拟rxjava1
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-29 22:18
 */
public class Main {
    public static void main(String[] args) {
        //拨打电话
        Calling calling = Caller.create(new Caller.OnCall<String>() {
            public void call(Receiver<String> stringReceiver) {
                //电话接通
                if (!stringReceiver.isUnCalled()) {
                    //发射数据
                    stringReceiver.onReceive("customer");
                    stringReceiver.onReceive("test");
                    stringReceiver.onCompleted();
                }
            }
        }).call(new Receiver<String>() {
            //通话结束
            public void onCompleted() {
                System.out.println("onCompleted");
            }

            //非主观原因通话失败
            public void onError(Throwable error) {
                System.out.println("onError");
            }

            //接收数据
            public void onReceive(String message) {
                System.out.println("onReceive:" + message);
            }
        });
        //挂断电话
        calling.unCall();
    }
}
