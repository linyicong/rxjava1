package cn.edu.neusoft.demo.topic3.customer;

/**
 * 简单调用 ——Action1
 *
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-29 22:21
 */
public interface Action1<T> {
    void call(T t);
}
